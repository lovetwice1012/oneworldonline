#!/usr/bin/env bash

export APACHE_LOCK_DIR=/tmp
export APACHE_RUN_USER=container
export APACHE_RUN_GROUP=container
export APACHE_LOG_DIR=log

if [ ! -d /home/container/.apache2/log ]; then
  mkdir -p /home/container/.apache2/log
fi

apache2 -f /home/container/.apache2/apache2.conf -DFOREGROUND
