#!/usr/bin/env bash
set -e

if [ ! -d ../.data/mysql ]; then
  mkdir -p ../.data/mysql
  mkdir -p ../.data/log/mysql
  initialize=true
fi

if [ "$(pgrep mysqld)" ]; then
  echo 'MySQL server already running'
  exit 0
fi

"C:\Program Files\MySQL\MySQL Server 8.0\bin\mysql" &
pid=$!

if [ "${initialize}" = "true" ]; then
  sleep 3
  "C:\Program Files\MySQL\MySQL Server 8.0\bin\mysql" -u root < ../oneworld.dump.txt
fi

# Transform SIGINT to SIGTERM
trap "{ kill -TERM ${pid}; }" INT
wait
